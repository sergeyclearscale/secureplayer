<!-- Include Radiant Media Player JavaScript file in your <body> or <head> -->
<script type="text/javascript" src="//cdn.radiantmediatechs.com/rmp/2.7.6/js/rmp.min.js"></script>
<!-- Set up your wrapper div with its unique id -->
<div id="rmpPlayer"></div>
<!-- Set up player configuration options -->
<?php
require_once __DIR__.'/TMVTokenHelper.php';

$config = require_once __DIR__.'/config.php';

$TMVTokenHelper = new TMVTokenHelper($config);

$bitrates = array(
  'hls' => array(
    'Auto' => 'http://52.9.146.23:1935/liveedge/smil:stream/manifest.m3u8'
  )
);

$bitrates = $TMVTokenHelper->transformUrlArray($bitrates);

foreach ($bitrates as &$bitratesSeries){
  $newBitratesSeries = array();
  foreach ($bitratesSeries as $bitrate => $url){
    $newBitratesSeries[] = array($bitrate, $url);
  }
  $bitratesSeries = $newBitratesSeries;
}
unset($bitratesSeries);

$jsBitrates = json_encode($bitrates, TRUE);
?>
<script type="text/javascript">
// First we specify bitrates to feed to the player

var bitrates = <?php echo $jsBitrates ;?>;

var settings = {
  licenseKey: 'Y2lrYWFvcW1tbyEqXyUyMjI/cm9tNWRhc2lzMzBkYjBBJV8q', // Add your license key
  bitrates: bitrates,
  delayToFade: 3000,
  width: 640,
  height: 360,
  poster: 'https://www.radiantmediaplayer.com/images/poster-rmp-showcase.jpg',
  isLive: true // Here we tell the player we are streaming live video content
};
var element = 'rmpPlayer';
var rmp = new RadiantMP(element);
rmp.init(settings);
</script>


<?php

class TMVTokenHelper{
  protected $wowzaSecureToken = 'mySharedSecret';
  protected $wowzaTokenPrefix = 'myTokenPrefix';
  protected $wowzaCustomParameter = 'myParameter';
  protected $clientIp = '127.0.0.1';
  
  
  public function __construct($config = array()){
    foreach (array(
      'wowzaSecureToken',
      'wowzaTokenPrefix',
      'wowzaCustomParameter',
      'clientIp'
    ) as $key){
      if (isset($config[$key])) {
        $this->$key = $config[$key];
      }
    }
  }
  
  public function generateHash($SecureParams){
    $contentPath = $SecureParams['ContentPath'];
    $wowzaCustomParameter = $SecureParams['customParameter'];
    $wowzaSecureTokenStartTime = $SecureParams['secureTokenStartTime'];
    $wowzaSecureTokenEndTime = $SecureParams['secureTokenEndTime'];
  
    $hashstr = $contentPath ."?".
      $this->wowzaSecureToken ."&".
      $this->wowzaTokenPrefix.'endtime='.$SecureParams['secureTokenEndTime'] ."&".
      $this->wowzaTokenPrefix.'starttime='.$SecureParams['secureTokenStartTime']; 
    
    $hash = hash('sha256', $hashstr, TRUE);
    $resultHash = strtr(base64_encode($hash), '+/', '-_');
    
    return $resultHash;
  }
  
  public function generateSecureParams($url){
    static $cache = array();
    $urlParts = parse_url($url);
    $contentPath = trim(dirname($urlParts['path']), '/');
    if (!isset($cache[$contentPath])){
      $SecureParams = array(
        'ContentPath' => $contentPath,
        //'customParameter' => $this->wowzaTokenPrefix . "CustomParameter=" . $this->wowzaCustomParameter,
        'customParameter' => $this->wowzaCustomParameter,
        'secureTokenStartTime' => '0',
        'secureTokenEndTime' => strtotime(date('d-m-Y H:i')) + 1800 //time() + 1800 //(7 * 24 * 60 * 60)
      );
      $SecureParams['hash'] = $this->generateHash($SecureParams);
      
      $cache[$contentPath] = $SecureParams;
    }
    
    return $cache[$contentPath];
  }
  
  public function transformUrl($url){
    $urlSegments = explode('?', $url);
    
    $url = array_shift($urlSegments);
    
    $urlQuery = implode('?', $urlSegments);
    
    $SecureParams = $this->generateSecureParams($url);
    
    $secureQuery = 
      //$this->clientIp ."&" .
      //$SecureParams['customParameter'] ."&".  
      $this->wowzaTokenPrefix ."endtime=".$SecureParams['secureTokenEndTime'] ."&".
      $this->wowzaTokenPrefix ."starttime=".$SecureParams['secureTokenStartTime'] ."&".
      $this->wowzaTokenPrefix ."hash=".$SecureParams['hash'];
    
    $url = $url ."?". ($urlQuery !== '' ? $urlQuery .'&' : '') . $secureQuery;
    
    return $url;
  }
  
  public function transformUrlArray(array $urlArray){
    foreach ($urlArray as &$Item){
      if (is_array($Item)){
        $Item = $this->transformUrlArray($Item);
      }else{
        $Item = $this->transformUrl($Item);
      }
    }
    return $urlArray;
  }
  
}

